package login

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

var lgnRepository = LoginRepository {}

func TestUserNotFound(t *testing.T) {
	creds, err := lgnRepository.getUserCredentials(loginRequest)
	t.Log(creds)
	assert.Nil(t, creds)
	assert.Equal(t, "User not found!", err.Error())
}

func TestUserFound(t *testing.T) {
	userName := "c137@onecause.com"
	validRequest := &LoginRequest {
		Username: userName,
		Password: "doesn't matter",
		Token: "doesnt' matter",
	}
	creds, err := lgnRepository.getUserCredentials(validRequest)
	t.Log(creds)
	assert.Equal(t, userName, creds.Username)
	assert.Nil(t, err)
}