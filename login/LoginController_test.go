package login

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

var lgnController LoginController

type loginServiceMock struct{}


var loginMock func(request *LoginRequest) (bool, error)

func (m loginServiceMock) Login(request *LoginRequest) (bool, error) {
	return loginMock(request)
}

func TestLoginSuccessful(t *testing.T) {
	mock := loginServiceMock {}
	loginMock = func(request *LoginRequest) (bool, error) {
		return true, nil
	}

	lgnController = LoginController {
		LoginService: mock,
	}
	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)	
	
	c.Request = createNewRequest()
	
	lgnController.Login(c)
	assert.Equal(t, 200, w.Code)
}


func TestLoginBadRequest(t *testing.T) {
	mock := loginServiceMock {}
	loginMock = func(request *LoginRequest) (bool, error) {
		return false, nil
	}

	lgnController = LoginController {
		LoginService: mock,
	}
	
	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)

	lgnController.Login(c)

	assert.Equal(t, 400, w.Code)
}

func TestLoginFailure(t *testing.T) {
	mock := loginServiceMock {}
	loginMock = func(request *LoginRequest) (bool, error) {
		return false, errors.New("Some error!")
	}

	lgnController = LoginController {
		LoginService: mock,
	}
	
	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	
	c.Request = createNewRequest()

	lgnController.Login(c)

	assert.Equal(t, 401, w.Code)
	
	expected := gin.H{
		"error":"Some error!",
	}
	
	var actual gin.H
	
	err := json.Unmarshal( w.Body.Bytes(), &actual)
    if err != nil {
        t.Fatal(err)
    }
	
	assert.Equal(t, expected, actual) 
}

func createNewRequest() *http.Request {
	validRequest := createValidLoginRequest()
	buf := new(bytes.Buffer)
	bytes, _ := json.Marshal(validRequest)
	buf.Write(bytes)
	 request, _ := http.NewRequest("POST", "/login", buf)
	 return request
}