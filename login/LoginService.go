package login

import (
	"errors"
	"strconv"
	"time"
)

type loginService interface {
	Login(request *LoginRequest) (bool, error)
}

type LoginService struct {
	LoginRepository loginRepository
}

// Should really be setting a header with a token we can validate to ensure future requests are validated
func (ls LoginService) Login(request *LoginRequest) (bool, error) {
	credentials, err := ls.LoginRepository.getUserCredentials(request)

	if err != nil {
		if err.Error() == "User not found!" {
			return false, errors.New("Invalid username or password!")
		}
		return false, err
	}

	if !isValidCredentials(request, credentials) {
		return false, errors.New("Invalid username or password!")
	}

	if !isValidToken(request) {
		return false, errors.New("Invalid token!")
	}

	return true, nil
}

func isValidCredentials(request *LoginRequest, credentials *UserCredentials) bool {
	return request.Username == credentials.Username && request.Password == credentials.Password
}

func isValidToken(request *LoginRequest) bool {
	return request.Token == createValidToken()
}


// Would be good to move token creation to its own service to all for mockablity
func createValidToken() string {
	now := time.Now()
	tokenString := strconv.Itoa(now.Hour())
	minutes := now.Minute()
	if minutes < 10 {
		tokenString = tokenString + "0" + strconv.Itoa(minutes)
	} else {
		tokenString = tokenString + strconv.Itoa(minutes)
	}

	return tokenString
}