package login

import "errors"

type loginRepository interface {
	getUserCredentials(request *LoginRequest) (*UserCredentials, error)
}

type LoginRepository struct{}

var credentials = []UserCredentials{UserCredentials{
	Username: "c137@onecause.com",
	Password: "#th@nH@rm#y#r!$100%D0p#",
}}

func (loginRepository LoginRepository) getUserCredentials(request *LoginRequest) (*UserCredentials, error) {
	for _, credential := range credentials {
		if credential.Username == request.Username {
			return &credential, nil
		}
	}

	return nil, errors.New("User not found!")
}
