package login

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type LoginController struct {
	LoginService loginService
}

func (loginController *LoginController) Login(c *gin.Context) {
	
	// Validate input
	var input LoginRequest
	if err := c.ShouldBindJSON(&input); err != nil {
	  c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	  return
	}

	result, err := loginController.LoginService.Login(&input)

	if err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": err.Error()})
		return
	}

	if result {
		c.Status(http.StatusOK)
	}
}