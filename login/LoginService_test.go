package login

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

var lgnService LoginService

type loginRepositoryMock struct{}

var getUserCredentialsMock func(request *LoginRequest) (*UserCredentials, error)

var loginRequest = &LoginRequest{
	Username: "user",
	Password: "pass",
	Token:    "1234",
}

func (m loginRepositoryMock) getUserCredentials(request *LoginRequest) (*UserCredentials, error) {
	return getUserCredentialsMock(request)
}

func TestSuccessfulLogin(t *testing.T) {
	var validLoginRequest = createValidLoginRequest()

	getUserCredentialsMock = func(request *LoginRequest) (*UserCredentials, error) {
		return &UserCredentials{
			Username: request.Username,
			Password: request.Password,
		}, nil
	}

	testLogin(t, true, nil, validLoginRequest)
}

func TestNoUserFound(t *testing.T) {
	err := errors.New("Invalid username or password!")
	getUserCredentialsMock = func(request *LoginRequest) (*UserCredentials, error) {
		return nil, errors.New("User not found!")
	}

	testLogin(t, false, err, loginRequest)
}

func TestPasswordDoesNotMatch(t *testing.T) {
	err :=  errors.New("Invalid username or password!")
	getUserCredentialsMock = func(request *LoginRequest) (*UserCredentials, error) {
		return &UserCredentials{
			Username: request.Username,
			Password: "nope",
		}, nil
	}

	testLogin(t, false, err, loginRequest)
}

func TestBubbleOtherErrorsUp(t *testing.T) {
	err := errors.New("Some random error!")
	getUserCredentialsMock = func(request *LoginRequest) (*UserCredentials, error) {
		return nil, err
	}

	testLogin(t, false, err, loginRequest)
}


func TestInvalidToken(t *testing.T) {
	err := errors.New("Invalid token!")
	getUserCredentialsMock = func(request *LoginRequest) (*UserCredentials, error) {
		return &UserCredentials{
			Username: request.Username,
			Password: request.Password,
		}, nil
	}

	testLogin(t, false, err, loginRequest)
}

func testLogin(t *testing.T, resultValue bool, errorValue error, r *LoginRequest) {
	mock := loginRepositoryMock{}

	lgnService = LoginService{
		LoginRepository: mock,
	}

	result, err := lgnService.Login(r)

	assert.Equal(t, resultValue, result)
	if errorValue == nil {
		assert.Nil(t, err)
	} else {
		assert.Equal(t, errorValue.Error(), err.Error())
	}
}

func createValidLoginRequest() *LoginRequest {
	return &LoginRequest{
		Username: "user",
		Password: "pass",
		Token: 	  createValidToken(),
	}	
}