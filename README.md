# OneCause Programming Exercise - API Server

Author - David Mast

This API server is written for my application to OneCause. The goal of this portion of the exercise is to validate requests for authentication from the frontend. 

## Reflections

Go presents some interesting changes in repository layout/packaging from what I am used to. After some research, I wasn't entirely sure what best practices were. I'm hoping what I have here isn't too crazy. 

I'd be interested in checking out how best to do integration testing as well. 

## Notes
* Tests are run on commit via BitBucket pipelines
* Swagger documentation for the API can be found in the `/api` folder

## Running

This application can be run via either `go build && ./onecause-backend.exe` or via a Docker container:
* `docker build -t onecause-backend`
* `docker run -p 8080:8080 --name onecause-backend onecause-backend:latest`