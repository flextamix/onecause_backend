FROM golang:alpine

ENV GIN_MODE=release
ENV PORT=8080
ENV TZ="America/New_York"

WORKDIR /app

COPY . /app

RUN go build

EXPOSE $PORT

ENTRYPOINT ["./onecause-backend"]