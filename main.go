package main

import (
	"fmt"
	"onecause-backend/login"

	"github.com/gin-gonic/gin"
)

func main() {
	// Dependancy creation/managment
	loginRepository := login.LoginRepository {}
	loginService := login.LoginService {
		LoginRepository: loginRepository,
	}
	loginController := login.LoginController {
		LoginService: loginService,
	}

	// Server setup
	r := gin.Default()
	r.POST("/api/v1/login", loginController.Login)
	err := r.Run(":8080")
	
	if(err != nil) {
		fmt.Printf("Error starting server: %s", err.Error())	
	}
}
